<?php

include_once('config/database.php');
class PhoneModel{

  private $db;

  public function __construct() {
    $this->db = new Database(
      'localhost',
      'agenda',
      'root',
      ''
    );

    $this->db = $this->db->connect();
  }

  public function createPhone($nro, $nroDes, $contact) {
    $sqlPhone = $this->db->prepare('INSERT INTO telefono(tel_nro, tel_des, con_id) VALUES(?,?,?)');
    $sqlPhone->execute(array($nro, $nroDes, $contact));

    $getLastPhone = $this->db->query('SELECT * FROM telefono ORDER BY tel_id DESC LIMIT 1');
    return $getLastPhone->fetchAll();

  }

  public function getPhonesByContact($id) {
    $sql = $this->db->query("
      SELECT * FROM telefono
      WHERE con_id = $id AND tel_sta = 1
    ");

    $phones = $sql->fetchAll();
    return $phones;
  }

  public function getOnePhone($id) {
    $sql = $this->db->query("
      SELECT * FROM telefono
      WHERE tel_id = $id AND tel_sta = 1
    ");

    $phone = $sql->fetchAll();
    return $phone;
  }

  public function editPhone($id, $nro, $des, $conId) {
    $sql = $this->db->query("
      UPDATE telefono
      SET tel_nro = '$nro', tel_des = '$des'
      WHERE tel_id = '$id'
    ");

    $phoneUpdate = $sql->fetchAll();

    $phone = $this->getPhonesByContact($conId);
    return $phone;
  }

  public function disablePhone($id) {
    $sql = $this->db->query("
      UPDATE telefono
      SET tel_sta = 0
      WHERE tel_id = $id
    ");

    $contactUpdate = $sql->fetchAll();
    return $contactUpdate;
  }


}