<?php

include_once('config/database.php');
class ContactsModel{

  private $db;

  public function __construct() {
    $this->db = new Database(
      'localhost',
      'agenda',
      'root',
      ''
    );

    $this->db = $this->db->connect();
  }

  public function getAllContacts($currentPage, $perPage) {
    $contactsList = [];
    $initialPage = ($currentPage - 1)*$perPage;
    $initialPage = intval($initialPage);
    $perPage = intval($perPage);

    $sqlCount = $this->db->query("SELECT COUNT(*) FROM agenda.contacto WHERE contacto.con_sta = 1");
    $countAllContacts = $sqlCount->fetchAll();

    $sql = $this->db->query("
      SELECT * FROM agenda.contacto
      JOIN correo on contacto.con_id = correo.con_id
      JOIN telefono on contacto.con_id = telefono.con_id
      WHERE contacto.con_sta = 1
      GROUP BY contacto.con_id
      LIMIT $initialPage,$perPage
    ");

    foreach($sql->fetchAll() as $contact) {
      $contactsList[] = $contact;
    }

    return [
      'contactsList' => $contactsList,
      'countAllContacts' => $countAllContacts
    ];
  }

  public function searchContacts($filterName, $filterCategory, $currentPage, $perPage) {
    $contactsList = [];
    $initialPage = ($currentPage - 1)*$perPage;
    $initialPage = intval($initialPage);
    $perPage = intval($perPage);

    $sqlCount;

    if($filterName !== '' && $filterCategory !== '') {
      $sqlCount = $this->db->query("
        SELECT COUNT(*) FROM agenda.contacto
        WHERE con_nom LIKE '%$filterName%' AND contacto.con_sta = 1 AND contacto.cat_id = '%$filterCategory%'
      ");
    }else if($filterName !== '' && $filterCategory === '') {
      $sqlCount = $this->db->query("
        SELECT COUNT(*) FROM agenda.contacto
        WHERE con_nom LIKE '%$filterName%' AND contacto.con_sta = 1
      ");
    }else if($filterName === '' && $filterCategory !== '') {
      $sqlCount = $this->db->query("
        SELECT COUNT(*) FROM agenda.contacto
        WHERE contacto.con_sta = 1 AND contacto.cat_id = '%$filterCategory%'
      ");
    }

    $countAllContacts = $sqlCount->fetchAll();

    $sql;

    if(!empty($filterName) && !empty($filterCategory)) {
      $sql = $this->db->query("
        SELECT * FROM agenda.contacto
        JOIN correo on contacto.con_id = correo.con_id
        JOIN telefono on contacto.con_id = telefono.con_id
        WHERE con_nom LIKE '%$filterName%' AND contacto.con_sta = 1 AND contacto.cat_id = $filterCategory
        GROUP BY contacto.con_id
        LIMIT $initialPage,$perPage
      ");
    }else if(!empty($filterName) && empty($filterCategory)) {
      $sql = $this->db->query("
        SELECT * FROM agenda.contacto
        JOIN correo on contacto.con_id = correo.con_id
        JOIN telefono on contacto.con_id = telefono.con_id
        WHERE con_nom LIKE '%$filterName%' AND contacto.con_sta = 1
        GROUP BY contacto.con_id
        LIMIT $initialPage,$perPage
      ");
    }else if(empty($filterName) && !empty($filterCategory)) {
      $sql = $this->db->query("
        SELECT * FROM agenda.contacto
        JOIN correo on contacto.con_id = correo.con_id
        JOIN telefono on contacto.con_id = telefono.con_id
        WHERE contacto.con_sta = 1 AND contacto.cat_id = $filterCategory
        GROUP BY contacto.con_id
        LIMIT $initialPage,$perPage
      ");
    }


    foreach($sql->fetchAll() as $contact) {
      $contactsList[] = $contact;
    }

    return [
      'contactsList' => $contactsList,
      'countAllContacts' => $countAllContacts
    ];
  }

  public function getOneContact($id) {
    $sql = $this->db->query("
      SELECT * FROM agenda.contacto
      WHERE con_id = $id AND con_sta = 1
    ");

    $contact = $sql->fetchAll();
    return $contact;
  }

  public function editContact($id, $name, $dh, $dt, $idCat) {
    $sql = $this->db->query("
      UPDATE contacto
      SET con_nom = '$name', con_dh = '$dh', con_dt = '$dt', cat_id = '$idCat'
      WHERE con_id = $id
    ");

    $contactUpdate = $sql->fetchAll();

    $contact = $this->getOneContact($id);
    return $contact;
  }

  public function disableContact($id) {
    $sql = $this->db->query("
      UPDATE contacto
      SET con_sta = 0
      WHERE con_id = $id
    ");

    $contactUpdate = $sql->fetchAll();
    return $contactUpdate;
  }

  public function createContact($name, $dh, $dt, $idCat) {
    $sqlContact = $this->db->prepare('INSERT INTO contacto(con_nom, con_dh, con_dt, cat_id) VALUES(?,?,?,?)');
    $sqlContact->execute(array($name, $dh, $dt, $idCat));

    $getLastContact = $this->db->query('SELECT * FROM contacto ORDER BY con_id DESC LIMIT 1');
    return $getLastContact->fetchAll();
  }


}