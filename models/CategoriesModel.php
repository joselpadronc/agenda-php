<?php

include_once('config/database.php');
class CategoriesModel{

  private $db;

  public function __construct() {
    $this->db = new Database(
      'localhost',
      'agenda',
      'root',
      ''
    );

    $this->db = $this->db->connect();
  }

  public function getAllCategories() {
    $categoriesList = [];

    $sql = $this->db->query("
      SELECT * FROM agenda.categoria
      WHERE categoria.cat_sta = 1
    ");

    foreach($sql->fetchAll() as $category) {
      $categoriesList[] = $category;
    }

    return $categoriesList;
  }

  public function getOneCategory($id) {
    $sql = $this->db->query("
      SELECT * FROM agenda.categoria
      WHERE cat_id = $id AND cat_sta = 1
    ");

    $category = $sql->fetchAll();
    return $category;
  }

  public function createCategory($name) {
    $sqlCategory = $this->db->prepare('INSERT INTO categoria(cat_nom) VALUES(?)');
    $sqlCategory->execute(array($name));

    $getLastCategory = $this->db->query('SELECT * FROM categoria ORDER BY cat_id DESC LIMIT 1');
    return $getLastCategory->fetchAll();
  }

}