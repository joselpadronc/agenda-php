<?php

include_once('config/database.php');
class EmailModel{

  private $db;

  public function __construct() {
    $this->db = new Database(
      'localhost',
      'agenda',
      'root',
      ''
    );

    $this->db = $this->db->connect();
  }

  public function createEmail($email, $emailDesc, $contact) {
    $sqlEmail = $this->db->prepare('INSERT INTO correo(cor_dir, cor_des, con_id) VALUES(?,?,?)');
    $sqlEmail->execute(array($email, $emailDesc, $contact));

    $getLastEmail = $this->db->query('SELECT * FROM correo ORDER BY cor_id DESC LIMIT 1');
    return $getLastEmail->fetchAll();
  }

  public function getOneEmail($id) {
    $sql = $this->db->query("
      SELECT * FROM correo
      WHERE cor_id = $id AND cor_sta = 1
    ");

    $phone = $sql->fetchAll();
    return $phone;
  }

  public function disableEmail($id) {
    $sql = $this->db->query("
      UPDATE correo
      SET cor_sta = 0
      WHERE cor_id = $id
    ");

    $contactUpdate = $sql->fetchAll();
    return $contactUpdate;
  }

  public function getEmailsByContact($id) {
    $sql = $this->db->query("
      SELECT * FROM correo
      WHERE con_id = $id AND cor_sta = 1
    ");

    $emails = $sql->fetchAll();
    return $emails;
  }

  public function editEmails($id, $dir, $des, $conId) {
    $sql = $this->db->query("
      UPDATE correo
      SET cor_dir = '$dir', cor_des = '$des'
      WHERE cor_id = '$id'
    ");

    $emailsUpdate = $sql->fetchAll();

    $email = $this->getEmailsByContact($conId);
    return $email;
  }


}