<?php require_once 'views/components/navbar.php'; ?>

<section class="container-fluid	 my-5 pb-5">
    <h2 class="text-center font-weight-bolder mb-5">Contactos</h2>
    <div class="w-100 mb-4 d-flex flex-wrap gap-2 justify-content-between">
      <form class="d-flex" action="/agenda-php/contacts/search" method="GET">
        <input class="form-control me-2 mr-2" type="text" placeholder="Buscar por nombre" name="name">
        <select class="form-select mr-2" aria-label="Seleccionar categoria" name="category">
          <option selected value="">Seleccionar categoria</option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select>
        <button class="btn btn-outline-success" type="submit">Buscar</button>
      </form>
      <?php
        if($_SESSION['user']['level'] === 'Administrador') {
          echo "
          <a href='/agenda-php/contacts/create' class='btn btn-primary'>
            Crear Contacto
          </a>
          ";
        } else {
          echo "";
        }
      ?>
    </div>
    <article class="p-4 bg-light rounded-3 overflow-auto">
      <?php if(count($contacts) !== 0) { ?>
        <table class="table w-100" style="min-width: 1024px;">
          <thead>
          <tr>
            <th scope="col">Nombre</th>
            <th scope="col">Nro Telefónico</th>
            <th scope="col">Correo</th>
            <th scope="col">Dirección</th>
            <th scope="col">Acción</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($contacts as $contact) { ?>
            <tr>
              <td class="text-truncate">
                <?php echo $contact['con_nom'] ?>
              </td>
              <td class="text-truncate">
                <?php echo $contact['cor_dir'] ?>
              </td>
              <td class="text-truncate">
                <?php echo $contact['tel_nro'] ?>
              </td>
              <td class="text-truncate">
                <?php echo $contact['con_dh'] ?>
              </td>
              <td>
                <?php
                  echo "<a href='/agenda-php/contacts/detail?id=".$contact['con_id']."'"." class='btn btn-sm btn-primary'>Ver</a>";
                ?>
              </td>
            </tr>
          <?php } ?>
          </tbody>
        </table>
      <?php } else { ?>
        <tr><h4>No hay informacion</h4></tr>
      <?php } ?>
    </article>
    <div class='d-flex justify-content-center align-items-center mt-4'>
      <div class="btn-group" role="group" aria-label="Basic example">
        <?php
          if(!empty($_GET['name'])) {
            if($currentPage > 1) {
              echo "<a style='width: 92px' href='/agenda-php/contacts/search?name=".$_GET['name']."&category=".$_GET['category']."&page=$prevPage' class='btn btn-primary'>Atras</a>";
            }else {
              echo "<a style='width: 92px' href='/' class='btn btn-primary disabled'>Atras</a>";
            }
            if($currentPage < $totalPage) {
              echo "<a href='/agenda-php/contacts/search?name=".$_GET['name']."&category=".$_GET['category']."&page=$nextPage' class='btn btn-primary'>Siguiente</a>";
            }else {
              echo "<a style='width: 92px' href='/' class='btn btn-primary disabled'>Siguiente</a>";
            }
          }else {
            if($currentPage > 1) {
              echo "<a style='width: 92px' href='/agenda-php/?page=$prevPage' class='btn btn-primary'>Atras</a>";
            }else {
              echo "<a style='width: 92px' href='/agenda-php/?page=$prevPage' class='btn btn-primary disabled'>Atras</a>";
            }
            if($currentPage < $totalPage) {
              echo "<a style='width: 92px' href='/agenda-php/?page=$nextPage' class='btn btn-primary'>Siguiente</a>";
            }else {
              echo "<a style='width: 92px' href='/' class='btn btn-primary disabled'>Siguiente</a>";
            }
          }
        ?>
      </div>
    </div>
</section>

<?php require_once 'views/components/footer.php'; ?>