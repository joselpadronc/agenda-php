<?php require_once 'views/components/navbar.php'; ?>
  <section class="container my-5 pb-5 pt-5">
    <h2 class="text-center font-weight-bolder mb-5">Iniciar sesion</h2>
    <div class="row mx-auto" style="max-width: 400px;">
      <div class="col mb-4 mx-auto">
        <article class="col-sm mx-auto bg-light py-3 px-4 rounded-lg shadow-sm">
          <form class="mt-4 form-group" action="/agenda-php/login" method="POST">
            <input
              class="form-control"
              type="text"
              placeholder="Nombre de usuario"
              name="user"
              id="user"
              required
            >
            <input
              class="form-control mt-2"
              type="password"
              placeholder="Contraseña"
              name="password"
              id="password"
              required
            >
            <button type="submit" class="btn btn-primary btn-block mt-4 mx-auto" id="auth">Entrar</button>
          </form>
        </article>
      </div>
    </div>
  </section>
<?php require_once 'views/components/footer.php'; ?>