<?php require_once 'views/components/navbar.php'; ?>

<section class="container my-5 pb-5">
    <div class="row mx-auto card rounded border-0 shadow" style="max-width: 600px;">
        <div class="col mb-4 mx-auto">
            <article class="col-sm mx-auto bg-light py-3 px-4 rounded-lg shadow-sm">
              <div class="d-flex align-items-start justify-content-between">
                <h3 class="fw-bold" id="name">
                  <?php echo $contact['con_nom'] ?>
                </h3>
                <?php
                  if($_SESSION['user']['level'] === 'Administrador') {
                    echo "
                    <div class='d-flex items-center gap-2'>
                      <a href='/agenda-php/contacts/edit?id={$contact['con_id']}' class='btn btn-outline-warning btn-sm p-0 px-2 mr-2 text-dark'>
                        Editar
                      </a>
                      <form action='#' method='POST'>
                        <input
                          type='hidden'
                          value='{$contact['con_id']}'
                          name='contactIdDelete'
                          id='contactId'
                        >
                        <button type='button' class='btn btn-sm p-0 px-2 text-danger' id='deleteContact'>
                          Eliminar
                        </button>
                      </form>
                    </div>
                    ";
                  } else {
                    echo "";
                  }
                ?>
              </div>
              <div class="mt-2">
                <h5 class="fs-6" id="dh"><strong>Dirección de habitación:</strong> <?php echo $contact['con_dh'] ?></h5>
                <h5 class="fs-6" id="dt"><strong>Dirección de trabajo:</strong> <?php echo $contact['con_dt'] ?></h5>
                <h5 class="fs-6" id="catId"><strong>Categoria:</strong>
                  <?php echo $contact['cat_id'] !== null ? $category[0]['cat_nom'] : 'No tiene'; ?>
                </h5>
              </div>

              <div class="d-flex flex-column align-items-start justify-content-between">
                <!-- Info Numeros -->
                <div class="mt-4 w-100">
                  <div class="d-flex align-items-start justify-content-between">
                    <h5 class="fw-bolder mr-4">Numeros:</h5>
                    <?php
                      if($_SESSION['user']['level'] === 'Administrador') {
                        echo "
                        <button type=button class='btn btn-outline-info btn-sm p-0 px-2 text-dark' data-bs-toggle=modal data-bs-target=#NumbersModal>
                          Agregar
                        </button>
                        ";
                      } else {
                        echo "";
                      }
                    ?>
                  </div>
                  <ul id='listPhones'>
                    <?php
                      foreach($phones as $phone) {
                        if($_SESSION['user']['level'] === 'Administrador') {
                          echo "
                            <li class='d-flex align-items-center my-2 w-100 justify-content-between'>
                              <div>{$phone['tel_nro']} - {$phone['tel_des']}</div>
                              <a href='/agenda-php/phones/delete?id={$phone['tel_id']}&contactId={$contact['con_id']}' class='btn ml-2'>
                                <img src='/agenda-php/images/icons/deleteIcon.svg' alt='Icon'/>
                              </a>
                            </li>
                          ";
                        } else {
                          echo "
                            <li class='d-flex align-items-center my-2 w-100 justify-content-between'>
                              <div>{$phone['tel_nro']} - {$phone['tel_des']}</div>
                            </li>
                          ";
                        }
                      }
                    ?>
                  </ul>
                </div>
                <!-- Info Emails -->
                <div class="mt-2 w-100">
                  <div class="d-flex align-items-start justify-content-between">
                    <h5 class="fw-bolder mr-4">Correos:</h5>
                    <?php
                      if($_SESSION['user']['level'] === 'Administrador') {
                        echo "
                        <button type='button' class='btn btn-outline-info btn-sm p-0 px-2 text-dark' data-bs-toggle='modal' data-bs-target='#EmailsModal'>
                          Agregar
                        </button>
                        ";
                      } else {
                        echo "";
                      }
                    ?>
                  </div>
                  <ul id='listEmails'>
                    <?php
                      foreach($emails as $email) {
                        if($_SESSION['user']['level'] === 'Administrador') {
                          echo "
                            <li class='d-flex align-items-center my-2 w-100 justify-content-between'>
                              <div>{$email['cor_dir']} - {$email['cor_des']}</div>
                              <a href='/agenda-php/emails/delete?id={$email['cor_id']}&contactId={$contact['con_id']}' class='btn ml-2'>
                                <img src='/agenda-php/images/icons/deleteIcon.svg' alt='Icon'/>
                              </a>
                            </li>
                          ";
                        } else {
                          echo "
                            <li class='d-flex align-items-center my-2 w-100 justify-content-between'>
                              <div>{$email['cor_dir']} - {$email['cor_des']}</div>
                            </li>
                          ";
                        }
                      }
                    ?>
                  </ul>
                </div>
              </div>
            </article>
        </div>
    </div>
</section>

<?php require_once 'views/components/emailsModal.php'; ?>
<?php require_once 'views/components/numbersModal.php'; ?>
<script src="/agenda-php/js/detailContact.js"></script>
<?php require_once 'views/components/footer.php'; ?>