  <footer class='p-2 mt-5 w-100 bg-footer'>
    <div class='d-flex justify-content-between container'>
      <img src="/agenda-php/images/ccec.png" width='85px' height='110px' alt="PCT Logo">
      <div class='text-center mt-4'>
        <h5 class='text-white'>Centro de control y comunicaciones</h5>
        <h2 class='text-white'>2021</h2>
      </div>
      <img src="/agenda-php/images/logopct.png" width='100px' height='110px' alt="PCT Logo">
    </div>
    <!-- <h5>Footer</h5> -->
  </footer>
  <script src="/agenda-php/js/bootstrap.bundle.min.js"></script>
  <script src="/agenda-php/js/jquery.min.js"></script>
  <script src="/agenda-php/js/login.js"></script>
  <script src="//widget.time.is/t.js"></script>
  <script>
    time_is_widget.init({_z120:{id:"San_Cristobal__Táchira_z120"}});
  </script>
  <script>
    window.onload = () => {
      let loading = document.getElementById('loader');
      if(loading){
        setTimeout(() => {
          loading.classList.replace('d-flex', 'd-none');
        }, 250);
      }
    }
  </script>
</body>
</html>