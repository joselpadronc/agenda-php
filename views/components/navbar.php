<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Agenda telefonica</title>
  <link rel="stylesheet" href="/agenda-php/css/style.css">
  <link rel="stylesheet" href="/agenda-php/css/bootstrap.css">
</head>
<body>
  <?php require_once 'views/components/loader.php'; ?>
  <div class='p-2 bg-dark text-white d-flex justify-content-center'>
    <h1>Sistema de Agenda Telefonica</h1>
  </div>
  <nav class="navbar navbar-expand-lg navbar-light bg-navbar" style='position: sticky; top: 0'>
    <div class="container-fluid">
      <div class='d-flex align-items-center'>
        <strong>
          <a class="navbar-brand text-white" href="/agenda-php/">Agenda Telefonica </a>
        </strong>
        <h6 class='text-white m-0 p-0'>
          Hora: <span id="_z120" style="font-size:20px" class='text-white'></span>
        </h6>
      </div>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse justify-content-end align-items-center" id="navbarNav">
        <ul class="navbar-nav">
          <?php
            if (isset($_SESSION['user'])) {
              if($_SESSION['user']['level'] === 'Administrador') {
                echo "
                  <li class='nav-item'>
                    <a class='nav-link text-light' aria-current='page' href='/agenda-php/'>Inicio</a>
                  </li>
                  <li class='nav-item'>
                    <a class='nav-link text-light' href='/agenda-php/categories'>Categorias</a>
                  </li>
                  <li class='nav-item'>
                    <a class='nav-link text-light' aria-current='page' href='/agenda-php/contacts/create'>Crear contacto</a>
                  </li>
                  <li class='nav-item dropdown'>
                    <a class='nav-link dropdown-toggle text-light' href='#' id='navbarDarkDropdownMenuLink' role='button' data-bs-toggle='dropdown' aria-expanded='false'>
                      ".$_SESSION['user']['username']."
                    </a>
                    <ul class='dropdown-menu dropdown-menu-end dropdown-menu-lg-start' style='left:-40px' aria-labelledby='navbarDarkDropdownMenuLink'>
                      <li><a class='dropdown-item' href='/agenda-php/login/logout'>Cerrar sesion</a></li>
                    </ul>
                  </li>
                ";
              } else {
                echo "
                  <li class='nav-item'>
                    <a class='nav-link text-light' aria-current='page' href='/agenda-php/'>Inicio</a>
                  </li>
                  <li class='nav-item dropdown'>
                    <a class='nav-link dropdown-toggle text-light' href='#' id='navbarDarkDropdownMenuLink' role='button' data-bs-toggle='dropdown' aria-expanded='false'>
                      ".$_SESSION['user']['username']."
                    </a>
                    <ul class='dropdown-menu dropdown-menu-end dropdown-menu-lg-start' style='left:-40px' aria-labelledby='navbarDarkDropdownMenuLink'>
                      <li><a class='dropdown-item' href='/agenda-php/login/logout'>Cerrar sesion</a></li>
                    </ul>
                  </li>
                ";
              }
            }
          ?>
        </ul>
      </div>
    </div>
  </nav>