<div class="modal fade" id="EmailsModal" tabindex="-1" aria-labelledby="EmailsModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Agregar otro Correo para el contacto</h5>
        <button type="button" class="btn btn-close d-flex align-center rounded-circle p-0 w-0 h-0" data-bs-dismiss="modal" aria-label="Close" id='close3'>
          <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-x-circle" viewBox="0 0 16 16">
            <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
            <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
          </svg>
        </button>
      </div>
      <div class="modal-body">
        <form action="#" method="POST">
          <input
            type="text"
            class="form-control mb-2"
            placeholder="Agregar Correo"
            name="inputEmailDir"
            id='inputEmailDir'
            required
          >
          <input
            type="text"
            class="form-control mb-2"
            placeholder="Descripcion del Correo"
            name="inputEmailDes"
            id='inputEmailDes'
            required
          >
          <button type="submit" class="btn btn-primary mt-4" id='saveEmail'>Guardar</button>
        </form>
      </div>
    </div>
  </div>
</div>