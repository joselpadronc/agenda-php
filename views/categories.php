<?php require_once 'views/components/navbar.php'; ?>
  <section class="container my-5 pb-5" style='height: 55vh'>
    <h2 class="text-center font-weight-bolder mb-5 fw-bold">Categorias</h2>
    <div class='d-flex align-items-start justify-content-center w-100 flex-wrap'>
      <article class="col-sm mx-auto bg-light py-3 px-4 rounded-lg shadow-sm" style='max-width: 350px !important;'>
        <h4 class="text-center font-weight-bolder mb-5 fw-bold">Crear</h4>
        <form class="form-group" action="/agenda-php/categories/create" method="POST">
          <div>
            <input
              class="form-control mb-2"
              type="text"
              placeholder="Nombre"
              name="name"
              id="name"
            >
          </div>
          <div class="w-100 d-flex justify-content-between align-items-center gap-3">
            <a href='/agenda-php/' class="btn btn-outline-secondary btn-block mt-4 mr-2">Cancelar</a>
            <button type="submit" class="btn btn-primary btn-block mt-4 ml-2" id="saveBtn">Guardar</button>
          </div>
        </form>
      </article>
      <article class="col-sm mx-auto bg-light py-3 px-4 rounded-lg shadow-sm" style='max-width: 350px !important;'>
        <h4 class="text-center font-weight-bolder mb-5 fw-bold">Lista</h4>
        <ul>
          <?php
            foreach($categories as $category) {
              echo "<li>{$category['cat_nom']}</li>";
            }
          ?>
        </ul>
      </article>
    </div>
  </section>
<?php require_once 'views/components/footer.php'; ?>