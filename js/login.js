const inputUser = document.getElementById('user');
const inputPassword = document.getElementById('password');
const btnSubmit = document.getElementById('auth');

if(btnSubmit !== null) {
  btnSubmit.addEventListener('click', (event) => {
    event.preventDefault();
    btnSubmit.setAttribute('disabled', "true");
    $.ajax({
      url: "http://localhost/agenda-php/login",
      type: "POST",
      data: {
        'username': inputUser.value,
        'password': inputPassword.value,
      }
    }).done(function(data){
      let response = JSON.parse(data);
      btnSubmit.removeAttribute('disabled');
      if(response.success === false) {
        alert(response.message);
      }else {
        alert(response.message);
        location.replace('/agenda-php/');
      }
    });
  })
}
