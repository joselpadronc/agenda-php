const BASE_URL = 'http://localhost/agenda-php';
let loading = document.getElementById('loader');
let contactId = document.getElementById('contactId');
let contactName = document.getElementById('inputName');
let contactDh = document.getElementById('inputDh');
let contactDt = document.getElementById('inputDt');
//
let phoneIdDelete = document.getElementById('phoneIdDelete');
let deleteContactBtn = document.getElementById('deleteContact');
//
let phoneNro = document.getElementById('inputTelNro');
let phoneDes = document.getElementById('inputTelDes');
let savePhoneBtn = document.getElementById('savePhone');
//
let emailDir = document.getElementById('inputEmailDir');
let emailDes = document.getElementById('inputEmailDes');
let saveEmailBtn = document.getElementById('saveEmail');
//
let modalCloseBtn1 = document.getElementById('close');
let modalCloseBtn2 = document.getElementById('close2');
let modalCloseBtn3 = document.getElementById('close3');

deleteContactBtn.onclick = (event) => {
  event.preventDefault();
  let messageConfirm = confirm('¿Esta seguro de elminar este recurso?');
  if(messageConfirm === true) {
    data = {
      'id': parseInt(contactId.value),
    }

    $.ajax({
      url: BASE_URL+"/contacts/delete",
      type: "POST",
      data: data
    }).done((data) => {
      let response = JSON.parse(data);
      if(response.success === false) {
        alert(response.message)
      }else {
        alert(response.message)
        location.replace('/agenda-php/')
      }
    })
  }
}

savePhoneBtn.onclick = (event) => {
  event.preventDefault();
  if(phoneNro.value === '' && phoneDes.value === '') {
    alert('Ingrese informacion en los campos');
  }else {
    savePhoneBtn.disabled = true;
    data = {
      'conId': parseInt(contactId.value),
      'nro': phoneNro.value,
      'des': phoneDes.value,
    }
    loading.classList.replace('d-none', 'd-flex');

    $.ajax({
      url: BASE_URL+"/phones/create",
      type: "POST",
      data: data
    }).done(function(data){
      let response = JSON.parse(data);
      savePhoneBtn.disabled = false;
      if(response.success === false) {
        alert(response.message)
      }else {
        const data =JSON.parse(response.data);
        const newLi = document.createElement('li');
        newLi.className = 'd-flex my-2 w-100 justify-content-between';
        newLi.style.maxWidth = '350px';
        newLi.innerHTML = `${data.tel_nro} - ${data.tel_des}`;
        const newNumber = newLi;
        document.getElementById('listPhones').appendChild(newNumber);
        modalCloseBtn2.click();
        alert(response.message);
        savePhoneBtn.disabled = false;
        loading.classList.replace('d-flex', 'd-none');
      }
    });
  }
}

saveEmailBtn.onclick = (event) => {
  event.preventDefault();
  if(emailDir.value === '' && emailDes.value === '') {
    alert('Ingrese informacion en los campos');
  }else {
    saveEmailBtn.disabled = true;
    data = {
      'conId': parseInt(contactId.value),
      'dir': emailDir.value,
      'des': emailDes.value,
    }
    loading.classList.replace('d-none', 'd-flex');

    $.ajax({
      url: BASE_URL+"/emails/create",
      type: "POST",
      data: data
    }).done(function(data){
      let response = JSON.parse(data);
      saveEmailBtn.disabled = false;
      if(response.success === false) {
        alert(response.message)
      }else {
        const data =JSON.parse(response.data);
        const newLi = document.createElement('li');
        newLi.className = 'd-flex my-2 w-100 justify-content-between';
        newLi.style.maxWidth = '350px';
        newLi.innerHTML = `${data.cor_dir} - ${data.cor_des}`;
        const newEmail = newLi;
        document.getElementById('listEmails').appendChild(newEmail);
        modalCloseBtn3.click();
        alert(response.message);
        saveEmailBtn.disabled = false;
        loading.classList.replace('d-flex', 'd-none');
      }
    });
  }
}