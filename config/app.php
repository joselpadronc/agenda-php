<?php
session_start();
class App {

  function __construct(){
    $url = isset($_GET['url']) ? $_GET['url'] : null;
    $url = rtrim($url, '/');
    $url = explode('/', $url);

    /*
    * Verifica si no hay palabras depues de la raiz (/) para mostrar una vista por defeto o buscar el controlador
    */
    if (empty($url[0])) {
      $controllerFile = 'controllers/HomeController.php';
      require_once $controllerFile;

      $controller = new HomeController();
      $controller->index();

      return false;
    }

    $controllerFile = 'controllers/'.ucfirst($url[0]).'Controller.php';

    // Verifica que el controlador exista, si no envia pagina 404
    if(file_exists($controllerFile)) {
      require_once $controllerFile;

      $controllerClass = ucfirst($url[0]).'Controller';

      $controller = new $controllerClass;

      /*
      * Verifica si hay un metodo para ejecutarlo, si no para ejecutar un metodo por defecto
      */
      if (isset($url[1])) {
        if(method_exists($controller, $url[1])) {
          $controller->{$url[1]}();
        }else {

        }
      }else {
        $controller->index();
      }

    }else {
      echo '<h1>Esta pagina no existe. Error 404</h1>';
    }

  }

}