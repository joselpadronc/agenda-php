<?php
  define('SERVER', '192.168.0.2');
  define('DOMAIN', 'inaprocet.gob.ve');
  define('DN', 'dc=inaprocet,dc=com,dc=ve');

  function loginWithActiveDirectory($user,$pass){
    $ldapUser = trim($user).'@'.DOMAIN;
    $ldapPass = trim($pass);
    $server = SERVER;
    $dn = DN;
    $puertoldap = 389;

    $ldapConn = @ldap_connect($server, $puertoldap);
    ldap_set_option($ldapConn, LDAP_OPT_PROTOCOL_VERSION, 3);
    ldap_set_option($ldapConn, LDAP_OPT_REFERRALS, 0);

    $ldapBind = @ldap_bind($ldapConn, $ldapUser, $ldapPass);

    if (!$ldapBind){
      return [
        'success' => false,
        'user' => $ldapBind
      ];
    }else{
      return [
        'success' => true,
        'user' => $ldapBind
      ];
    }
    ldap_close($ldapConn);
  }
?>