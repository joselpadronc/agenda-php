<?php
include_once("models/EmailModel.php");
class EmailsController {

  public function __construct () {
    $this->emailModel = new EmailModel();
  }

  public function create(){
    if(!isset($_SESSION['user'])){
      header('Location: /agenda-php/login');
      exit;
    }else {
      $emailUpdated = $this->emailModel->createEmail($_POST['dir'], $_POST['des'], $_POST['conId']);
      echo json_encode([
        'success' => true,
        'message' => 'Se guardo la informacion correctamente',
        'data' => json_encode($emailUpdated[0])
      ]);
    }
  }

  public function delete(){
    if(!isset($_SESSION['user'])){
      header('Location: /agenda-php/login');
      exit;
    }else {
      if($_GET['id']) {
        $phone = $this->emailModel->getOneEmail($_GET['id']);
        $phone = $phone[0];
        $phoneUpdated = $this->emailModel->disableEmail($_GET['id']);
        header("Location: /agenda-php/contacts/detail?id={$_GET['contactId']}");
        exit;
      }else {

      }
    }
  }

}