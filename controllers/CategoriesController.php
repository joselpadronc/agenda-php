<?php
include_once("models/CategoriesModel.php");
class CategoriesController {

  public function __construct () {
    $this->CategoriesModel = new CategoriesModel();
  }

  public function index(){
    if(!isset($_SESSION['user'])){
      header('Location: /agenda-php/login');
      exit;
    } else {
      $categories = $this->CategoriesModel->getAllCategories();

      require_once "views/categories.php";
    }
  }

  public function create(){
    if(!isset($_SESSION['user'])){
      header('Location: /agenda-php/login');
      exit;
    } else {
      $this->CategoriesModel->createCategory($_POST['name']);

      header('Location: /agenda-php/categories');
      die();
    }
  }
}