<?php
include_once("models/PhoneModel.php");
class PhonesController {

  public function __construct () {
    $this->phoneModel = new PhoneModel();
  }

  public function create(){
    if(!isset($_SESSION['user'])){
      header('Location: /agenda-php/login');
      exit;
    }else {
      $contactUpdated = $this->phoneModel->createPhone($_POST['nro'], $_POST['des'], $_POST['conId']);
      echo json_encode([
        'success' => true,
        'message' => 'Se guardo la informacion correctamente',
        'data' => json_encode($contactUpdated[0])
      ]);
    }
  }

  public function delete(){
    if(!isset($_SESSION['user'])){
      header('Location: /agenda-php/login');
      exit;
    }else {
      if($_GET['id']) {
        $phone = $this->phoneModel->getOnePhone($_GET['id']);
        $phone = $phone[0];
        $phoneUpdated = $this->phoneModel->disablePhone($_GET['id']);
        header("Location: /agenda-php/contacts/detail?id={$_GET['contactId']}");
        exit;
      }else {

      }
    }
  }

}