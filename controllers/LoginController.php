<?php
include_once("models/LoginModel.php");
include_once("config/activeDirectory.php"); 

class LoginController{

  private $loginModel;

  public function __construct () {
    $this->loginModel = new LoginModel();
  }


  public function index(){
    if(!isset($_SESSION['user'])){
      if ($_POST) {
        $usr = $_POST["username"];
        $usuario = loginWithActiveDirectory($usr, $_POST["password"]);
        $logguedUser = $this->loginModel->getUser($usr);
        if(!empty($logguedUser)) {
          if($usuario["success"] == false){
              $_SERVER = array();
              $_SESSION = array();
              echo json_encode([
                'success' => false,
                'message' => 'Usuario o contraseña incorrecta',
              ]);
          }else{
            $sessionInfo = [
              'level' => $logguedUser[0]['des_car'],
              'username' => $logguedUser[0]['nom_usu'],
            ];
            $_SESSION["user"] = $sessionInfo;
            echo json_encode([
              'success' => true,
              'message' => 'Bienvenid@',
            ]);
          }
        }else {
          echo json_encode([
            'success' => false,
            'message' => 'Usuari@ no registrado en el sistema',
          ]);
        }
      }else {
        require_once "views/login.php";
      }
    }else {
      header('Location: /agenda-php/');
      exit;
    }
  }

  public function logout(){
    session_destroy();
    header("Location: /agenda-php/login");
  }

}