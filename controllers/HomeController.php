<?php
include_once("models/ContactsModel.php");
class HomeController {

  public function __construct () {
    $this->contactsModel = new ContactsModel();
  }

  public function index(){
    if(!isset($_SESSION['user'])){
      header('Location: /agenda-php/login');
      exit;
    } else {
      $perPage = 15;
      $currentPage = !empty($_GET['page']) ? $_GET['page'] : 1;
      $contactsQuery = $this->contactsModel->getAllContacts($currentPage, $perPage);
      $contacts = $contactsQuery['contactsList'];
      $contactsCount = $contactsQuery['countAllContacts'][0][0];
      $totalPage = ceil($contactsCount / $perPage);

      $nextPage = $totalPage > 1 ? $currentPage + 1 : 1;
      $prevPage = $totalPage > 1 ? $currentPage - 1 : 1;

      require_once "views/home.php";
    }
  }

}