<?php

include_once("models/ContactsModel.php");
include_once("models/CategoriesModel.php");
include_once("models/EmailModel.php");
include_once("models/PhoneModel.php");
class ContactsController {

  private $contactsModel;
  private $emailsModel;
  private $phonesModel;

  public function __construct () {
    $this->contactsModel = new ContactsModel();
    $this->categoriesModel = new CategoriesModel();
    $this->emailsModel = new EmailModel();
    $this->phonesModel = new PhoneModel();
  }

  public function create(){
    if(!isset($_SESSION['user'])){
      header('Location: /agenda-php/login');
      exit;
    }else {
      if($_POST) {
        $name = $_POST['name'];
        $dh = $_POST['dh'];
        $dt = $_POST['dt'];
        $idCat = $_POST['idCat'];
        $email = $_POST['correoDir'];
        $emailDesc = $_POST['correoDes'];
        $nroTel = $_POST['telNro'];
        $nroTelDesc = $_POST['telDes'];

        $contact = $this->contactsModel->createContact($name, $dh, $dt, $idCat);

        foreach($email as $key => $emailItem){
          $this->emailsModel->createEmail($emailItem, $emailDesc[$key], $contact[0]['con_id']);
        }

        foreach($nroTel as $key => $phoneItem){
          $this->phonesModel->createPhone($phoneItem, $nroTelDesc[$key], $contact[0]['con_id']);
        }

        header("Location: /agenda-php/contacts/detail?id={$contact[0]['con_id']}");
        die();
      }else {
        $categories = $this->categoriesModel->getAllCategories();
        require_once "views/createContact.php";
      }
    }
  }

  public function detail(){
    if(!isset($_SESSION['user'])){
      header('Location: /agenda-php/login');
      exit;
    }else {
      if($_GET['id']) {
        $contact = $this->contactsModel->getOneContact($_GET['id']);
        if (count($contact) === 0) {
          echo '<h1>Esta pagina no existe. Error 404</h1>';
        }else {
          $category = '';
          $contact = $contact[0];
          if($contact['cat_id'] !== null) {
            $category = $this->categoriesModel->getOneCategory($contact['cat_id']);
          }
          $emails = $this->emailsModel->getEmailsByContact($_GET['id']);
          $phones = $this->phonesModel->getPhonesByContact($_GET['id']);
          require_once "views/detailContact.php";
        }
      }else {
        header("Location: /agenda-php/");
        die();
      }
    }
  }

  public function edit(){
    if(!isset($_SESSION['user'])){
      header('Location: /agenda-php/login');
      exit;
    }else {
      if($_GET['id']) {
        $contact = $this->contactsModel->getOneContact($_GET['id']);
        if (count($contact) === 0) {
          echo '<h1>Esta pagina no existe. Error 404</h1>';
        }else {
          $categories = $this->categoriesModel->getAllCategories();
          $contact = $contact[0];
          $emails = $this->emailsModel->getEmailsByContact($_GET['id']);
          $phones = $this->phonesModel->getPhonesByContact($_GET['id']);
          require_once "views/editContact.php";
        }
      }else {
        header("Location: /agenda-php/");
        die();
      }
    }
  }

  public function update(){
    if(!isset($_SESSION['user'])){
      header('Location: /agenda-php/login');
      exit;
    }else {
      $email = $_POST['correoDir'];
      $emailId = $_POST['correoId'];
      $emailDesc = $_POST['correoDes'];
      $nroTel = $_POST['telNro'];
      $nroTelId = $_POST['telId'];
      $nroTelDesc = $_POST['telDes'];

      $contactUpdate = $this->contactsModel->editContact($_POST['id'], $_POST['name'], $_POST['dh'], $_POST['dt'], $_POST['idCat']);

      foreach($email as $key => $emailItem){
        $emailUpdate = $this->emailsModel->editEmails($emailId[$key], $emailItem, $emailDesc[$key], $_POST['id']);
      }

      foreach($nroTel as $key => $phoneItem){
        $phoneUpdate = $this->phonesModel->editPhone($nroTelId[$key], $phoneItem, $nroTelDesc[$key], $_POST['id']);
      }

      header('Location: /agenda-php/contacts/detail?id='.$_POST['id']);
      die();
    }
  }

  public function search(){
    if(!isset($_SESSION['user'])){
      header('Location: /agenda-php/login');
      exit;
    }else {
      if($_GET['name'] || $_GET['category']) {
        print_r($_GET['name'] .'-    -'. $_GET['category']);
        $perPage = 15;
        $currentPage = !empty($_GET['page']) ? $_GET['page'] : 1;
        $contactsQuery = $this->contactsModel->searchContacts($_GET['name'], $_GET['category'], $currentPage, $perPage);
        $contacts = $contactsQuery['contactsList'];
        $contactsCount = $contactsQuery['countAllContacts'][0][0];
        $totalPage = ceil($contactsCount / $perPage);

        $nextPage = $totalPage > 1 ? $currentPage + 1 : 1;
        $prevPage = $totalPage > 1 ? $currentPage - 1 : 1;
        require_once "views/home.php";
      }else {
        header("Location: /agenda-php/");
        die();
      }
    }
  }

  public function delete(){
    if(!isset($_SESSION['user'])){
      header('Location: /agenda-php/login');
      exit;
    }else {
      $contact = $this->contactsModel->getOneContact($_POST['id']);
      if (count($contact) === 0) {
        echo json_encode([
          'success' => false,
          'message' => 'Contacto no existe'
        ]);
      }else {
        $contact = $contact[0];
        $contactUpdated = $this->contactsModel->disableContact($_POST['id']);
        echo json_encode([
          'success' => true,
          'message' => 'Se elmino correctamente'
        ]);
      }
    }
  }

}